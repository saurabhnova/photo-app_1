//
//  FaceDetection.h
//  PhotoApp
//
//  Created by vineet on 06/08/13.
//  Copyright (c) 2013 JID Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface FaceDetection : NSObject
{
    ViewController *delegate;
}

@property(strong,nonatomic) ViewController *delegate;


-(void)markFaces:(NSArray *)imageArray;
-(UIImageView *)faceDetector:(NSString *)imageName withDelegate:(id)delegateObj withFrame:(CGRect)imageFrame;

@end
