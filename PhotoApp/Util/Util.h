//
//  Util.h
//  PhotoApp
//
//  Created by vineet on 06/08/13.
//  Copyright (c) 2013 JID Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject



+(UILabel *)createLabel:(NSString *)name withFrame:(CGRect)frame withTextColor:(UIColor *)textColor withBackgroundColor:(UIColor *)backgroundColor withFontSize:(CGFloat)size withLines:(NSInteger)lines;
+(UIButton *)createButton:(NSString *)title withType:(UIButtonType)type withFrame:(CGRect)frame withFontSize:(CGFloat)size withTextColor:(UIColor*)textColor withBackgroundColor:(UIColor *)BackgroungColor withTag:(NSInteger)tag;
+(CGFloat)resizeLableToFitWith:(UILabel*)lbl;
+(CGFloat)expectedHeightWithUilable:(UILabel*)lblObj;
+(UIScrollView *)createScrollViewWithTag:(NSInteger)tag withFrame:(CGRect)frame withContentWidth:(CGFloat)contentWidth withContentHeight:(CGFloat)contentHeight withVerticalIndicator:(BOOL)vertical withHorizontalIndicator:(BOOL)horizontal withPaging:(BOOL)paging;
+(UIAlertView *)createAlertView:(NSString *)title withMessage:(NSString *)message;
+(UIImageView *)createImageView:(NSString *)image withFrame:(CGRect)frame withTag:(NSInteger)tag;



@end
